STEAM Camp with Hawila on Isefjord
==================================

*Theme:*
A bucolic setting consisting of an old wooden ship laying at anchor off the shore of a 19th century farm house.

*Date:* June 29th - July 3rd 2020

*Instructors:*
 * Adam – Tree climbing
 * Alexandre – Welding
 * Alix – STEM
 * Ditlev – Sailing
 * Emanuel – Cooking
 * Emmanuelle – Cooking & logistics
 * Gabriele – Cooking
 * George – Photography
 * Joséphine – STEM & cooking
 * Mette – Graphical art
 * Nel – STEM and textile printing
 * Pauline – Sailing
 * Sacha – STEM
 * Samuel – Hawila activities and STEM
 * Stefan – STEM

*Sponsors:*
 * The Franco-Danish School
 * Hawila Project

[Organization / schedules](schedules.md)

[Equipment list for the participants](what-to-bring.md)

[List of suggested improvements after the camp](improvements.md)

![Camp Poster](steamcamp-summer-20-poster.jpg)
