| Id | Suggestions for improvements for the camp next time (or already now) | Reporter | Notes |
| -- | -------------------------------------------------------------------- | -------- | ----- |
| 1 | Check List of materials | Stefan | (STEM sessions in particular) |
| 2 | Task list  / chore teams | Nicolas |  |
| 3 | Drikkedunke | Manue |  |
| 4 | Workbags (Materials & worksheets) for Steam workshops | Stefan |  |
| 5 | Clearly define responisiblity of children | Stefan |  |
| 6 | Make parents work with their children in the workshops | Stefan |  |
| 7 | Make presentations | Mette |  |
| 8 | Have two full sailing days with Hawila + tech workshops on those days (e.g. electronics and music) | Nicolas / Sam | Place according to weather forecast |
| 9 | Keep: repeated workshops, improvement over days | Jon |  |
| 10 | Keep: parents joining their kids during the workshops, eases teacher's job  + root knowledge in family) | Stefan |  |
| 11 | More sailing days with Hawila | Vincent | And Aurore |
| 12 | Mix activities Hawila / farm (e.g. remote-controlled sails, robots) | Emanuel |  |
| 13 | Morning meeting - program of the day | Jon | Morning song |
| 14 | Dedicated PR team | Nicolas | Should be on chore list |
| 15 | Dinghy on a rope between Hawila and the shore | Adam |  |
| 16 | Fewer options in the workshops | Niels |  |
| 17 | Pre-camp STEM intro course for the parents | Nel |  |
| 18 | Whiteboard in a shared place with info (each child, chores) | Niels | Should be a blackboard! |
| 19 | Pre-camp meeting for the instructors, e.g. 3 weeks prior to camp | Emanuel |  |
| 20 | Work group: Emanuel, Adam, Jon, Nel, Stefan, Kim | Nicolas | and Sam and Nicolas |
| 21 | Clear workshop objectives defined prior to camp, shared among instructors. This will allow each instructor to more readily refer to and build upon the learnings from other workshop in his or her own workshop. | Jon | relates to parents pre-meeting |
| 22 | Evening meal for everyone at the farm | Sam |  |
| 23 | Order food by truck to the farm | Gabriele |  |
| 24 | Buy fridge on DBA | Gabriele |  |
| 25 | Keep: encourage parents to make workshops | Sam | making the stay free for instance |
| 26 | Swimming more when on the Hawila | Heidi-Rose |  |
| 27 | More communication to people out from the school, to have more participant from outside. | Aurore |  |
| 28 | Create something across the first days which is used or performed the last day. Ideas: Write a song and some music to perform at the end of the camp. A stone oven to prepare pizza for the last evening.  | Jon |  |
| 29 | Buy a guitar | Djamel | on DBA |
| 30 | More Dark Side Challenges + tower toppling | Nicolas |  |
|  |
