List of things to bring to the camp
===================================

 * A tent, unless you stay on the Hawila
 * A t-shirt or two print a logo on (preferably pure cotton)
 * Your laptop
 * Your robot(s), drones, Arduinos etc.
 * Bicycles for the excursions
 * Possibly life jackets
