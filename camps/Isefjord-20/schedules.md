Schedules
=========
Daily
-----
 * From 8:00: breakfast
 * 10:00 – 12:00: First workshop
 * 11:00 – 13:00: Lunch & fun
 * 13:00 – 15:00: Second workshop
 * 15:00 : Excursion
 * 18:00: Dinner

Weekly
------

| Day | Track 1: Courtyard | Track 2: Stable | Track 3: Hawila | Track 4: Garden | Track 5: Shore | Track 6: Excursions |
| --- | ------------------ | --------------- | --------------- | --------------- | ---------------| ------------------- |
| Day 1 AM | Welcome & Planning |  |  |  |  |  |
| Day 1 PM | Welding | Print a T-shirt | Hawila Challenges | Tree climbing | Dinghy | Rå mælk og grøntsager fra gården |
| Day 2 AM | Welding | N-body problems | Holbæk Art |  | Dinghy |  |
| Day 2 PM | Soldering | Build a robot | Holbæk |  | Dinghy | Hansen ismejeri |
| Day 3 AM | Soldering | Hack a logo / Gimp | Hawila Challenges |  | Dinghy |  |
| Day 3 PM | Rygning af fx bacon | Hack a logo / Gimp | Hawila Challenges | Rover in the field | Dinghy | Hornbeer bryggeri |
| Day 4 AM | The Field Field Kit |  | Marine Electronics |  | Dinghy |  |
| Day 4 PM |  | Analog photography | Music workshop | Tree climbing | Dinghy | Selsø slot |
| Day 5 AM | Rover in the field | Rover in the field | Hawila Challenges |  | Dinghy |  |
| Day 5 PM |  | Print a T-shirt | Hawila Challenges |  | Dinghy | Ådalen |
