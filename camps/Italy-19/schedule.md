| Date | Time | Location | Description | Notes |
| ---- | ---- | -------- | ----------- | ----- |
| 2019-10-14 | 08:00:00 | Cph | Departure | Flight nr. EJU2654 / 9:45 EXQL7V5, EXQL7DR, EXRJZK4 |
| 2019-10-14 | 11:50:00 | Milano Malpensa | Arrival | Car rental: Keddy by Europcar Booking ID: 725879726, 722160911 |
| 2019-10-14 | 14:00:00 | Turin | Free time / Monday afternoon | AirBnB: Via Giovanni Napione 23+25 |
| 2019-10-14 | 20:00:00 | Turin | Meet up with the team from Supplyframe | Grand Hotel Sitea, Via Carlo Alberto, 35 |
| 2019-10-15 | 09:30:00 | Arduino HQ | Visit | ToolBox Coworking, Via Agostino da Montefeltro, 2 |
| 2019-10-15 | 12:00:00 | Lunch |   |   |
| 2019-10-15 | 14:00:00 | Arduino Factory | Visit | Strambino |
| 2019-10-15 |   | Turin | Free time / Tuesday evening |   |
| 2019-10-16 | 07:30:00 | Torino | Departure |   |
| 2019-10-16 | 11:30:00 | Larciano | Drop off luggage at farm house | AirBnB: Via Biccimurri 373D, Larciano |
| 2019-10-16 | 12:30:00 | Vinci | Visit da Vinci museum | http://www.museoleonardiano.it/eng |
| 2019-10-17 | 08:00:00 |   | Departure |   |
| 2019-10-17 | 12:00:00 | Rome | Arrival / Drop off luggage | AirBnB: Via di San Francesco a Ripa, 18 + Vicolo della Renella, 95 |
| 2019-10-17 | 14:00:00 | Nuova Fiera Roma | Set up booth | Nuova Fiera di Roma |
| 2019-10-17 | 17:45:00 | Guido Reni District | Groundbreakers – Re:making the future | Via Guido Reni, 7 / Participants: Nicolas, Sacha |
| 2019-10-17 |   |   | Free time? or not... |   |
| 2019-10-18 | 09:00:00 | Nuova Fiera Roma | MFR19 |   |
| 2019-10-18 |   |   | Free time / Visit Rome |   |
| 2019-10-19 | 09:00:00 | Nuova Fiera Roma | MFR19 |   |
| 2019-10-19 |   |   | Free time / Visit Rome |   |
| 2019-10-20 | 09:00:00 | Nuova Fiera Roma | MFR19 |   |
| 2019-10-20 | 12:00:00 | Nuova Fiera Roma | Departure for Milan Malpensa |   |
| 2019-10-20 | 19:00:00 | Milan Malpensa | Arrival | Idea Hotel Milano Malpensa Airport |
| 2019-10-20 |   |   | Free time? |   |
| 2019-10-21 | 05:15:00 | Milan Malpensa | Leave Hotel |   |
| 2019-10-21 | 07:10:00 | Milan Malpensa | Departure for Cph | Flight nr. EJU2653 |
| 2019-10-21 | 09:15:00 | Cph | Arrival |   |
