Italian Tech Tour 2019
======================

*Theme:*
This camp was a precursor to the STEAM Camps and was organized by The Franco-Danish School as a study trip sponsored by Arduino, Supplyframe, Mouser and Microchip.

It was a road trip starting with a visit to Arduino's headquarter and factory in Turin, followed by a visit to the da Vinci museum in Vinci and ending with 3 days of STEM workshops at Maker Faire Rome.

![MFR 2019](crowdedbooth.jpg)

*Date:* 2019-10-14 - 2019-10-21

*Workshops*
 * Dark Side Challenge
 * Arduino Functions
 * Differential Equations
 * Jewelry Soldering
 * Storytelling and Graphics Design

*Instructors:*
 * Adam
 * Alix
 * Joséphine
 * Mette
 * Sacha
 * Stefan

*Sponsors:*
 * Arduino
 * Microchip.
 * Mouser 
 * Supplyframe
 * The Franco-Danish School

[Schedule](schedule.md)

![Visit Arduino Factory](visit-arduino-factory.jpg)
