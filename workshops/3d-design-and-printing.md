Designing and Printing 3D Models
================================
During the workshop, the participants design 3D objects using OpenSCAD, a text-based programming language.

![3D printing](images/3d-printing-289x512.jpg)

*Objectives:*
 * spark the interest for transforming an abstract idea into a materialized object
 * understand how iterations are generally needed to achieve a satisfying end result
 * learn about geometry in 3D, geometric primitives and boolean operations
 * get the participants acquainted with text-based programming
 
*Activitites:*
 * complete the OpenSCAD tutorial
 * design something, possibly a name tag
 * use a slicer to produce the gcode needed by the printer
 * print something. Name tags are relatively fast to print
 * experience getting hypnotized by the movements of the printer

Note: the 3D printing usually takes quite a bit of time, which will generally create a bottleneck at the printer. If the camp spans multiple days, use the night hours for printing larger jobs.

*Equipment:*
 * a computer running OpenSCAD
 * handouts with the tutorial
 * slicing softare
 * a 3D printer

*Evaluation criteria:*
 * number of participants
 * fraction completing the tutorial
 * fraction of participants setting up the whole design - slicing - printing tool chain
 * number of actual prints performed

*Follow-up activities:*
 * design and print all kinds of things at home
 * build a 3D printer from a kit
 * develop the Dark Side Rover concept by making an own remotely programmed robot
 * learn to use git by contributing to the existing collection of 3D models
 * build 3D models and animations using Blender
 * build architectural 3D models using Sween Home 3D

*Resources*
 * [OpenSCAD](http://openscad.org)
 * [Prusa slicer](https://www.prusa3d.com/prusaslicer/)
 * [OpenSCAD beginner's tutorial](https://gitlab.com/ecolefrancodanoise/3d-printing-efd/-/blob/master/Handouts/Beginner/Beginner_OpenSCAD.pdf)
 * [Collection of 3D-models for inspiration](https://gitlab.com/ecolefrancodanoise/3d-printing-efd)
 * [Blender](http://blender.org)
 * [Sweet Home 3D](http://www.sweethome3d.com)
