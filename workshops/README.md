The STEAM Camp workshops
========================

The workshops are the core of the STEAM Camps. They typically take the form of permanent stations that the participants can attend.

[The Dark Side Challenge](dark-side-challenge.md)

The goal of the Dark Side Challenge is to build a robot that can autonomously navigate through a terrain where radio contact is sporadic or impossible – just as if it were on the dark side of the Moon or somewhere even farther away.

![Dark Side Challenge](images/darksidetowers.jpg)


[The Arduino Functions Workshop](arduino-functions.md)

This workshop illustrates the concepts of sensors and actuators using a simple microcontroller.

![Arduino functions](images/sensoractuatorsdemo.jpg)


[Playing with Differential Equations](differential-equations.md)

Differential equations (DEs) are about solving complex problems by engaging them from one end and working your way through it, integrating one small piece (difference) at the time into the eventually complete solution.

![N-body problem](images/nebula.jpg)

[3D design and printing](3d-design-and-printing.md)

Design 3D objects using OpenSCAD, a text-based programming language and possibly print them.

![3D printing](images/3d-printing-289x512.jpg)
