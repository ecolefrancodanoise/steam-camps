Hackaton
========
Within the hacker communities, a so-called *hackathon* (the contraction of *hacking* and *marathon*) is a well-known concept which consists of meeting up and getting some intensive programming done, typically during a day or so.

The hackaton or hackatonne (/hæk.ə.tʌn/) however is a less known concept consisting of hacking an actual metric ton of devices.

While the hackathon is supposed to be a relatively short and intense event during which you are supposed to get something useful done, there are no such constraints on the hackaton. Reaching the full metric ton of hacks can take weeks, months or even years. Also, the definition of a hack is quite broad and diffuse, but can essentially be summarized to “any modification or use of an object that was not originally intended”, leaving a lot of freedom in the choice of activities.

![Hackaton details](images/hackaton.jpg)

*Objectives*
 * illustrate that most devices consist of the same building blocks
 * initiate or strengthen the habit of investigating what lies behind the surface.
 * practicing simple motor skills such as screwing (clock-wise) and unscrewing (counter-clock-wise)

*Activities:*
 * disassemble devices
 * reassemble devices, possibly adding new functionality with a microcontroller

*Equipment:*
 * Screw drivers and other tools
 * Some Arduinos, wires and miscellaneous components

*Evaluation criteria:*
 * number of participants
 * tons of devices dismantled
 * tons of devices reassembled

*Follow-up activities:*
 * disassemble stuff at home, scavenge and sort the components into a "library"

![Hackaton group](images/hackaton2.jpg)
