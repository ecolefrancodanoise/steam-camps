Soldering and Electronic Jewelry
================================
Using copper wires, LEDs and a soldering iron, design beautiful jewelry.

![Electronic jewelry](images/electronic-jewelry.jpg)

*Objectives:*
 * introduce soldering in a fun and engaging way
 * get acquainted with LEDs, conductors, batteries, voltage and current

*Activities:*
 * bend the copper wire, attach the LEDs and the battery and see the jewelry light up. Magic!
 * fry a LED using a battery with a low internal resistance to illustrate the concept of a short circuit

Note: Motor skill-wise and soldering skill-wise this workshop is not for beginners and can get very time-consuming for the instructor.

*Equipment:*
 * soldering iron and solder
 * copper wire
 * LEDs
 * batteries (typically CR2032 (high internal resistance) and AA (low resistance))

*Evaluation criteria:*
 * number of participants
 * number of working pieces of jewelry
 * shortness of the queueing line

*Follow-up activities:*
 * Add a microcontroller and make all kinds of wearable electronics

*Resources*
 * [Inspiring models](https://jiripraus.cz/svitici-led-sperky/)
