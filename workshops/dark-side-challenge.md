The Dark Side Challenge
=======================

The goal of the Dark Side Challenge is to build a robot that can autonomously navigate through a terrain where radio contact is sporadic or impossible – just as if it were on the dark side of the Moon or somewhere even farther away.

While achieving that goal, the challengers are learning to program and debug embedded devices and sensors, to imagine, reason and persevere, understand how to properly balance machine intelligence with human interaction. The youngest ones are even learning the alphabet – both in capital and small letters!

![Dark Side Challenge](images/darksidetowers.jpg)

The workshop is suitable for nearly all ages (3+), the follow-up activities are more 7+ (with assistance).

*Objectives:*
 * get the participants acquainted with the concept of sending a series of commands to a robot, typically with a fun purpose 
 * give the participants an understanding of how information such as photos can be sent to the internet 

*Activities:*
 * building a challenge, for instance building a tower that can then be toppled by the rover
 * program the rover to solve the challenge, typically by sending a string of movement commands to it (w - forward, d - backwards, a - turn left, s - turn right)

*Equipment:*
 * a Dark Side Rover with recharged batteries
 * spare batteries and charger
 * a Dark Side Server, typically on the internet
 * a wifi access point for the rover to connect to the server
 * a computer that can send commands to the server
 * anything required to set up a challenge to solve, for instance bricks to build a tower

*Evaluation parameters:*
 * number of participants
 * did the participants use the rover's camera to solve the challenge?
 * were there any technical challenges, for instance with the wifi, motors jamming etc.?

*Follow-up activities:*
 * build own rover and program it
 * contribute to the existing Dark Side Challenge code base

*Resources:*
 * [Hackaday project](https://hackaday.io/project/164082-the-dark-side-challenge)
 * [Code repository](https://gitlab.com/ecolefrancodanoise/arduino-efd/-/tree/master/darkside/)
 * [3D-models](https://gitlab.com/ecolefrancodanoise/3d-printing-efd/-/tree/master/OpenSCAD/DarkSideRover)
