Playing with Differential Equations
===================================

![N-body problem](images/nebula.jpg)

Differential equations (DEs) are about solving complex problems by engaging them from one end and working your way through it, integrating one small piece (difference) at the time into the eventually complete solution.

In addition to being the archetype of computational thinking, DEs are also widely used to model all kinds of phenomena in practice and are also the subject of extended theoretical studies.

*Objectives:*
 * have fun making an animation
 * understand how something (state) changes when variables change
 * get acquainted with text-based programming

*Activities:*
 1. Make a simple time-varying system in p5 by copying a 20-line script into Khan Academy’s visual p5 interpreter
 1. Model a planetary orbit following an example on a handout, then modify and play with the different constants and variables

It is interesting to note that the vast majority of children (from about 6+) have the patience to spell their way through the script and truly get a beginning understanding of the concept by playing with the variables.

*Equipment:*
 * a computer with access to a visual p5 interpreter ([Khan Academy](https://www.khanacademy.org/computer-programming/new/pjs), [p5js.org](https://editor.p5js.org/), in-browser)
 * [handouts](https://gitlab.com/ecolefrancodanoise/de-for-children/-/blob/master/processingjs/progression-example/handouts-danish/lektion01.md) with two sample programs as exercises
 
*Evaluation criteria:*
 * number of participants
 * number of handouts distributed
 * fraction of participants finishing exercise 1
 * fraction of participants finishing exercise 2
 * did the participants get aha experiences, such as "Ah, this is how gravity works!"?

*Follow-up activities:*
 * play with the many examples in the [on-line progression](https://gitlab.com/ecolefrancodanoise/de-for-children/tree/master/processingjs/progression-example)
 
*Resources*
 * [examples of DEs](https://gitlab.com/ecolefrancodanoise/de-for-children) solving different problems and written in different languages
