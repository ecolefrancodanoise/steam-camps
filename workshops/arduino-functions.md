The Arduino Functions Workshop
==============================

This workshop illustrates the concepts of sensors and actuators using a simple microcontroller.

![Arduino functions](images/sensoractuatorsdemo.jpg)

*Objectives:*
 * have fun by making the microcontroller react to an input
 * get acquainted with wiring electronics using a breadboard
 * begin to understand that a *function* expresses how its *output* depends on its *input*, and nothing else
 * get acquainted with text-based programming

*Activities:*
 * try out the different sensor-actuator setups and explain what the sensor is measuring
 * modify a program that writes some text to an OLED screen depending on the input
 
*Equipment:*
 * a microcontroller, for instance an Arduino Uno
 * servos (as actuators)
 * sensors (light sensor, temperature, sound, distance,...)
 * an OLED screen as another example of an output
 
*Evaluation criteria:*
 * number of participants
 * fraction of participants doing the code-writing exercise

*Follow-up activities:*
 * build things using servos and actuators, for instance a rover that stops when it is close to objects

*Resources*
 * a collection of simple [Arduino programs](https://gitlab.com/ecolefrancodanoise/arduino-efd) for inspiration
