The STEAM Camps
===============
The STEAM Camps are educational events aimed at promoting scientific reasoning and computational thinking.

The objectives of the camps are to prime the kids with a positive experience of STEAM and initiate some rooting of the concepts within the families. The rooting can then be developed through on-line material and remote teaching programs.

Through a series of [hands-on DIY activities](workshops), the participants solve relevant and engaging technological challenges and acquire tools to understand and contribute to the rapid development society is undergoing.

A camp typically consists of a number of stations:
 * an area to assemble and test robots
 * a soldering and jewelry corner
 * a 3D-printing workshop
 * a math and programming corner
 * a corner for story-telling and graphic design
 * and many others...

Revolving around the core workshops, all kinds of interesting activities can be arranged, for instance sailing, climbing, discovering the neighborhood, kayaking, attending rocket launches etc.

The camps are also an occasion to meet up and strengthen inter-personal relations, as well as discovering the world and visit fantastic places, co-organized with people who know the whereabouts.

Organizing a camp
-----------------
Everyone is free to organize a camp and should consider the following steps:
 1. read through the material from previous camps
 1. decide on where and when to hold the camp
 1. identify which workshops to organize and find the personnel to drive them
 1. formulate a budget and possibly find sponsors
 1. announce the event wherever relevant and register subscriptions
 1. arrange accommodation and excursions
 1. after the camp: arrange an evaluation session and identify improvements for the next camps
